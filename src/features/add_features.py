import glob
import os

import click
import pandas as pd
from tqdm import tqdm


def add_features_to_file(file, input_directory, output_directory):
    """
    Add features to the file
    :param file: filename of the file to add features
    :param input_directory: input directory
    :param output_directory: output directory
    :return:
    """
    # Load data
    df = pd.read_parquet(file)
    # Add new features
    df["cam_in_ICEFRAC_minus_cam_in_LANDFRAC"] = (
        df["cam_in_ICEFRAC"] - df["cam_in_LANDFRAC"]
    )
    output_filename = output_directory + file.replace(input_directory, "")
    os.makedirs(os.path.dirname(output_filename), exist_ok=True)
    df.to_parquet(output_filename)


@click.command()
@click.argument("input_directory", type=click.Path(exists=True))
@click.argument("output_directory", type=click.Path())
def add_features(input_directory: str, output_directory: str):
    """
    Add new features to the dataset
    :param input_directory: directory with .parquet files
    :param output_directory: output directory with cleaned data
    :return:
    """
    # find filenames with *.parquet with glob
    files = glob.glob(os.path.join(input_directory, "*.parquet"), recursive=True)
    os.makedirs(output_directory, exist_ok=True)
    for file in tqdm(files):
        add_features_to_file(file, input_directory, output_directory)


if __name__ == "__main__":
    add_features()
