import logging

import click
import os
import glob

import numpy as np
import pandas as pd
from tqdm import tqdm

UNPREDICTABLE_TARGET_COLUMNS = (
    ["ptend_q0001_" + str(i) for i in range(12)]
    + ["ptend_q0002_" + str(i) for i in range(12)]
    + ["ptend_q0003_" + str(i) for i in range(12)]
    + ["ptend_u_" + str(i) for i in range(12)]
    + ["ptend_v_" + str(i) for i in range(12)]
)


def preprocess_and_save_file(filename, input_directory, output_directory):
    """
    Preprocess file
    :param filename: filename of the file to preprocess
    :param input_directory: input directory
    :param output_directory: output directory
    :return:
    """
    df = pd.read_parquet(filename)
    df = preprocess_dataframe(df)
    output_filename = output_directory + filename.replace(input_directory, "")
    os.makedirs(os.path.dirname(output_filename), exist_ok=True)
    df.to_parquet(output_filename)


def preprocess_dataframe(df):
    # Drop columns with 1 unique value
    df = df.drop(columns=df.columns[df.nunique() == 1])
    # Drop unpredictable targets
    df = df.drop(columns=UNPREDICTABLE_TARGET_COLUMNS, errors="ignore")
    number_of_states = 60
    df = df.drop(columns=[f"state_q0002_{i}" for i in range(12)], errors="ignore")
    # Normalize data
    for i in range(number_of_states):
        if f"state_q0002_{i}" not in df.columns:
            logging.warning(f"state_q0002_{i} not in columns")
            continue
        mean = df[f"state_q0002_{i}"].mean()
        std = df[f"state_q0002_{i}"].std()
        df[f"state_q0002_{i}"] = ((df[f"state_q0002_{i}"] - mean) / std).astype(
            "float32"
        )
        # fill inf values with mean
        df[f"state_q0002_{i}"] = df[f"state_q0002_{i}"].replace([np.inf, -np.inf], mean)
    return df


@click.command()
@click.argument("input_directory", type=click.Path())
@click.argument("output_directory", type=click.Path())
def clean_data(input_directory: str, output_directory: str):
    """
    Preprocess data for ML pipeline
    :param input_directory: directory with .parquet files
    :param output_directory: output directory with cleaned data
    :return:
    """
    # find filenames with *.parquet with glob
    files = glob.glob(os.path.join(input_directory, "*.parquet"), recursive=True)
    os.makedirs(output_directory, exist_ok=True)
    for file in tqdm(files):
        preprocess_and_save_file(file, input_directory, output_directory)


if __name__ == "__main__":
    clean_data()
