import click
import kaggle
import os


@click.command()
@click.argument("dataset_name", type=click.STRING)
@click.argument("output_filepath", type=click.Path())
def download_dataset_from_kaggle(dataset_name: str, output_filepath: str):
    """
    Download dataset from Kaggle to the raw directory to perform ML pipeline
    :param dataset_name: dataset name of dataset on Kaggle (for example titericz/leap-dataset-giba)
    :param output_filepath: directory, where dataset will be saved
    :return:
    """
    os.makedirs(output_filepath, exist_ok=True)
    kaggle.api.dataset_download_files(
        dataset_name, path=output_filepath, force=True, quiet=False, unzip=True
    )


if __name__ == "__main__":
    download_dataset_from_kaggle()
