from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="An intelligent, low cost spill detection and monitoring system designed to reduce the amount of spills in metallurgy industry",
    author="Astute",
    license="MIT",
)
