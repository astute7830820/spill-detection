import pytest
import pandas as pd
import numpy as np
from src.data.clean_data import preprocess_dataframe, UNPREDICTABLE_TARGET_COLUMNS


@pytest.fixture
def sample_dataframe():
    df = pd.DataFrame(
        {
            "constant_col": [1, 1, 1, 1],
            "ptend_q0001_0": [5, 6, 7, 8],
            "ptend_q0002_1": [9, 10, 11, 12],
            "ptend_q0003_2": [13, 14, 15, 16],
            "ptend_u_0": [17, 18, 19, 20],
            "ptend_v_1": [21, 22, 23, 24],
            "state_q0002_0": [1.0, 2.0, 3.0, 4.0],
            "state_q0002_1": [5.0, 6.0, 7.0, 8.0],
        }
    )
    for i in range(2, 60):
        df[f"state_q0002_{i}"] = np.random.rand(4)
    return df


def test_drop_constant_columns(sample_dataframe):
    df_processed = preprocess_dataframe(sample_dataframe.copy())
    assert "constant_col" not in df_processed.columns


def test_drop_unpredictable_targets(sample_dataframe):
    df_processed = preprocess_dataframe(sample_dataframe.copy())
    for col in UNPREDICTABLE_TARGET_COLUMNS:
        assert col not in df_processed.columns


def test_normalize_columns(sample_dataframe):
    df_processed = preprocess_dataframe(sample_dataframe.copy())
    for i in range(60):
        col = f"state_q0002_{i}"
        if col in df_processed.columns:
            mean = df_processed[col].mean()
            std = df_processed[col].std()
            assert np.isclose(mean, 0, atol=1e-5)
            assert np.isclose(std, 1, atol=1e-5)


if __name__ == "__main__":
    pytest.main()
